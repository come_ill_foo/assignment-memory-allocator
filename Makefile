CFLAGS=--std=c17 -Wall -pedantic -Isrc/main -ggdb -Wextra -Werror -DDEBUG
CLFLAGS=-fpic
BUILDDIR=build
LIBDIR=libs
SRCDIR=src
MAINDIR=$(SRCDIR)/main
TESTDIR=$(SRCDIR)/test
TESTS=tests
LIBNAME=cifmem
CC=gcc

all: test_mem0 test_mem1 test_mem2 test_mem3 test_mem4

build:
	mkdir -p $(BUILDDIR)

libs:
	mkdir -p $(LIBDIR)

test:
	mkdir -p $(TESTS)

$(LIBDIR)/lib$(LIBNAME).so: $(BUILDDIR)/mem.o $(BUILDDIR)/util.o $(BUILDDIR)/mem_debug.o
	$(CC) -shared -o $@ $^

test_%: $(TESTDIR)/test_%.c $(BUILDDIR)/log.o 
	$(CC) $(CFLAGS) -L$(PWD)/$(LIBDIR) $^ -o $(TESTS)/$@ -l$(LIBNAME)

$(BUILDDIR)/mem.o: $(MAINDIR)/mem.c build libs
	$(CC) -c $(CFLAGS) $(CLFLAGS) $< -o $@

$(BUILDDIR)/mem_debug.o: $(MAINDIR)/mem_debug.c build libs
	$(CC) -c $(CFLAGS) $(CLFLAGS) $< -o $@

$(BUILDDIR)/util.o: $(MAINDIR)/util.c build libs
	$(CC) -c $(CFLAGS) $(CLFLAGS) $< -o $@

$(BUILDDIR)/log.o: $(TESTDIR)/log.c $(LIBDIR)/lib$(LIBNAME).so test build
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -rf $(BUILDDIR)
	rm -rf $(LIBDIR)
	rm -rf $(TESTS)

