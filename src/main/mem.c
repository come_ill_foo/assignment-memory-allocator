#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
/**
 * the great assumptions begins...
 * 1. maybe need to recalculate region size
 * 'cause its rounded by the page size
 * 2. maybe need to map the region
 * 3. maybe the default for extends field is true
 * 4. maybe we should check if the region
 * is invalid before return it
 * 5. maybe block_init must be provided with
 * the region capacity translated into the block size
 */
static struct region alloc_region  ( void const * addr, size_t query ) {
  /*  ??? */
  const size_t region_size = region_actual_size( query );
  void* mapped_address = map_pages( addr, region_size, 0 ); /* check if zero is valid parameter for map */
   

  const struct region allocated = {
    .addr = mapped_address,
    .size = region_size,
    .extends = true
  };

  /* maybe we should check if the region is invalid before return it */
  if ( region_is_invalid( &allocated ) ) return REGION_INVALID;

  /* initialize memory region with the block */
  block_init( mapped_address, size_from_capacity( ( block_capacity ) { .bytes = region_size } ), NULL );
  return allocated;
}

static void* block_after( struct block_header const* block ) ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

/**
 * suppose the return value
 * is the success of splitting
 * 1. maybe we should check if block is splittable
 * 2. if not then return false
 * 3. otherwise split it
 * 4. calculate the next block address
 * 5. calculate the blocks new capacitites
 * 6. init the next block
 * 7. edit the old one to convenient values
 * 8. return success
 */
static bool split_if_too_big( struct block_header* block, size_t query ) {
  /*  ??? */
  if ( block_splittable( block, query ) ) {
    
    /* then need to calculate the first block's new capacity */
    const block_capacity prev_blk_cap = { .bytes = query };

    /* and next block's capacity */ 
    const block_size next_blk_size = ( block_size ) { .bytes = block->capacity.bytes - prev_blk_cap.bytes };

    /* edit the old block capacity */
    block->capacity = prev_blk_cap;

    /* maybe we need to calculate the next block address */
    void* next_blk_addr = block_after( block );

    /* lastly, init next block */
    block_init( next_blk_addr, next_blk_size, block->next );

    /* edit the old block next address */
    block->next = next_blk_addr;
    /* maybe it's all to split the block */
    return true;
  } else return false; /* block is unsplittable */
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return snd != NULL && fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  /* check if both blocks are free */
  if( block != NULL && block->next != NULL && mergeable( block, block->next ) ) {
    /* merging... */
    block->capacity.bytes += size_from_capacity( block->next->capacity ).bytes;
    block->next = block->next->next;
    return true;
  } else return false;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};

/**
 * 1. maybe create the iterator
 * 2. maybe check the block size and the required size
 * 3. maybe return the good block if true
 * 4. maybe otherwise, try to merge with the next block
 * 5. maybe if not successful then go to the next block
 * 6. maybe otherwise, check the merged block again
 */
static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
  /*??? */
  struct block_header* blk_iter = block;
  while ( blk_iter ) {
    while( try_merge_with_next( blk_iter ) );
    if ( blk_iter->is_free && block_is_big_enough( sz, blk_iter ) ) { /* condition for 'good' block */
      // split_if_too_big( blk_iter, sz ); /* maybe we should check the return value */
      return ( struct block_search_result ) { .type = BSR_FOUND_GOOD_BLOCK, .block = blk_iter };
    } else if ( !( mergeable( blk_iter, blk_iter->next ) && try_merge_with_next( blk_iter ) ) ) {
      if ( !blk_iter->next ) break;
      blk_iter = blk_iter->next;
    }
  }

  return ( struct block_search_result ) { .type = BSR_REACHED_END_NOT_FOUND, .block = blk_iter };
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  struct block_search_result good_or_last = find_good_or_last( block, query );
  if ( good_or_last.type == BSR_FOUND_GOOD_BLOCK ) {
      split_if_too_big( good_or_last.block, query );
      good_or_last.block->is_free = false;
  }

  return good_or_last; 
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  /*  ??? */
  /* maybe allocate new memory region with query size */ 
  const struct region new_region = alloc_region( block_after( last ), query );
  last->next = ( struct block_header* ) new_region.addr;
  struct block_header * new_last = last->next;
  return new_last;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {

  /*  ??? */
  struct block_search_result first_try = find_good_or_last( heap_start, query );
  switch ( first_try.type ) {
    case BSR_FOUND_GOOD_BLOCK:
      first_try.block->is_free = false;
      return first_try.block; break;

    case BSR_REACHED_END_NOT_FOUND: {
      struct block_header* new_last = grow_heap( first_try.block, size_from_capacity( ( block_capacity ) { query } ).bytes );
      struct block_search_result second_try = try_memalloc_existing( query, new_last );
      switch ( second_try.type ) {
        case BSR_FOUND_GOOD_BLOCK:
          second_try.block->is_free = false;
          return second_try.block; break;

        case BSR_REACHED_END_NOT_FOUND: /* just stub; i don't know what to do here */

        case BSR_CORRUPTED:
          return NULL;
      }
      break; 
    }

    case BSR_CORRUPTED:
      return NULL;
  }
  return NULL;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  /*  ??? */
  while ( try_merge_with_next( header ) );
}
