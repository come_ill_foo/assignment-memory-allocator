#include <stdio.h>
#include <stdint.h>
#include <mem.h>

#include "log.h"

#define INITIAL_SIZE 4

int main( int argc, char** argv ) {
    if ( argc > 1 )
        warn( "%s: additional parameters unsupported yet\n", argv[ 0 ] );

    info( "%s: started\n", argv[ 0 ] );
    void* heap_addr = heap_init( INITIAL_SIZE );
    info( "heap_addr is %p\n", heap_addr );
    debug_heap( stdout, heap_addr );

    info( "started allocating variables\n" );
    int* first = _malloc( sizeof( int ) );
    float* second = _malloc( sizeof( float ) );
    info( "finished allocating variables\n" );
    debug_heap( stdout, heap_addr );

    info( "started setting values for variables\n" );
    *first = 42;
    *second = 3.1415926f;
    info( "finished setting values for variables\n" );
    debug_heap( stdout, heap_addr );

    info( "started freeing variables\n" );
    _free( first );
    _free( second );
    info( "finished freeing variables\n" );
    debug_heap( stdout, heap_addr );

    info( "%s: finished\n", argv[ 0 ] );
    return 0;
} 