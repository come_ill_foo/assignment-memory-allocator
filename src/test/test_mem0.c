#include <stdio.h>
#include <mem.h>

#include "log.h"

#define INITIAL_SIZE 4196

int main( int argc, char** argv ) {
    if ( argc > 1 )
        warn( "%s: additional parameters unsupported yet\n", argv[ 0 ] );

    info( "%s: started\n", argv[ 0 ] );
    void* heap_addr = heap_init( INITIAL_SIZE );
    info( "heap_addr is %p\n", heap_addr );
    debug_heap( stdout, heap_addr );

    int* var = _malloc( sizeof( int ) );
    debug_heap( stdout, heap_addr );
    *var = -INITIAL_SIZE;
    info( "[ var = %d ]\n", *var );
    debug_heap( stdout, heap_addr );
    
    _free( var );
    debug_heap( stdout, heap_addr );

    info( "%s: finished\n", argv[ 0 ] );
    return 0;
} 