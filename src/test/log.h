#ifndef _LOG_H_
#define _LOG_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include "colours.h"

void info( const char* msg, ... );

void warn( const char* msg, ... );

#endif /* _LOG_H_ */