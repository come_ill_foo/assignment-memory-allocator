#include <stdio.h>
#include <stdint.h>
#include <mem.h>

#include "log.h"

#define INITIAL_SIZE 8192

int main( int argc, char** argv ) {
    if ( argc > 1 )
        warn( "%s: additional parameters unsupported yet\n", argv[ 0 ] );

    info( "%s: started\n", argv[ 0 ] );
    void* heap_addr = heap_init( INITIAL_SIZE );
    info( "heap_addr is %p\n", heap_addr );
    debug_heap( stdout, heap_addr );

    info( "started allocating variables\n" );
    void* a = _malloc( 7000 );
    debug_heap( stdout, heap_addr );
    void* b = _malloc( 3000 );
    debug_heap( stdout, heap_addr );
    void* c = _malloc( 8000 );
    info( "finished allocating variables\n" );
    debug_heap( stdout, heap_addr );

    info( "started freeing variables\n" );
    _free( a );
    debug_heap( stdout, heap_addr );
    _free( b );
    debug_heap( stdout, heap_addr );
    _free( c );
    info( "finished freeing variables\n" );
    debug_heap( stdout, heap_addr );

    info( "%s: finished\n", argv[ 0 ] );
    return 0;
} 