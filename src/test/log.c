#include "log.h"

void info( const char* msg, ... ) {
  va_list args;
  va_start( args, msg );
  fprintf( stderr, COLOUR( ANSI_COLOR_GREEN, "info: " ) );
  vfprintf( stderr, msg, args );
  va_end ( args ); 
}

void warn( const char* msg, ... ) {
  va_list args;
  va_start( args, msg );
  fprintf( stderr, COLOUR( ANSI_COLOR_YELLOW, "warn: " ) );
  vfprintf( stderr, msg, args );
  va_end ( args ); 
}